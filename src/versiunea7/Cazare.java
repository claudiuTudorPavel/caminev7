package versiunea7;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name="Cazari")
public class Cazare implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="idCazare")
	private Integer idCazare;
	//@Temporal(DATE)
	@Column(name="dataOraCazare")
	private int dataOraCazare;
	private String observatii;
	
	@ManyToOne
	@JoinColumn(name="idCamera")
	private Camera camera;
	@ManyToOne
	@JoinColumn(name="idCamin")
	private Camin camin;
	@ManyToOne
	@JoinColumn(name="idStudent")
	private Studenti studenti;
	
	public Cazare(){}

	public Integer getIdCazare() {
		return idCazare;
	}
	public void setIdCazare(Integer idCazare) {
		this.idCazare = idCazare;
	}
	public int getDataOraCazare() {
		return dataOraCazare;
	}
	public void setDataOraCazare(int i) {
		this.dataOraCazare = i;
	}
	public String getObservatii() {
		return observatii;
	}
	public void setObservatii(String observatii) {
		this.observatii = observatii;
	}
	public Camera getCamera() {
		return camera;
	}
	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	public Camin getCamin() {
		return camin;
	}
	public void setCamin(Camin camin) {
		this.camin = camin;
	}
	public Studenti getStudenti() {
		return studenti;
	}
	public void setStudenti(Studenti studenti) {
		this.studenti = studenti;
	}

	public Cazare(Integer idCazare, int dataOraCazare, String observatii, Camera camera, Camin camin, Studenti studenti) {
		super();
		this.idCazare = idCazare;
		this.dataOraCazare = dataOraCazare;
		this.observatii = observatii;
		this.camera = camera;
		this.camin = camin;
		this.studenti = studenti;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((camera == null) ? 0 : camera.hashCode());
		result = prime * result + ((camin == null) ? 0 : camin.hashCode());
		result = prime * result + dataOraCazare;
		result = prime * result + (int) (idCazare ^ (idCazare >>> 32));
		result = prime * result + ((observatii == null) ? 0 : observatii.hashCode());
		result = prime * result + ((studenti == null) ? 0 : studenti.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cazare other = (Cazare) obj;
		if (camera == null) {
			if (other.camera != null)
				return false;
		} else if (!camera.equals(other.camera))
			return false;
		if (camin == null) {
			if (other.camin != null)
				return false;
		} else if (!camin.equals(other.camin))
			return false;
		if (dataOraCazare != other.dataOraCazare)
			return false;
		if (idCazare != other.idCazare)
			return false;
		if (observatii == null) {
			if (other.observatii != null)
				return false;
		} else if (!observatii.equals(other.observatii))
			return false;
		if (studenti == null) {
			if (other.studenti != null)
				return false;
		} else if (!studenti.equals(other.studenti))
			return false;
		return true;
	}

	
	
	

	

}
