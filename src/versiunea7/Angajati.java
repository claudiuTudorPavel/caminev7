package versiunea7;


import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Angajati")
public class Angajati extends Persoane implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="marca")
	private Integer marca;
	@Column(name="vechime")
	private Integer vechime;
	@Column(name="tipAngajat")
	private String tipAngajat;
	
	@OneToOne
	@JoinColumn(name="idAngajat")
	private Persoane persoane;

	
	
	

	

	



	public Angajati()
	{}

	

	public Integer getMarca() {
		return marca;
	}

	public void setMarca(Integer marca) {
		this.marca = marca;
	}

	public Integer getVechime() {
		return vechime;
	}

	public void setVechime(Integer vechime) {
		this.vechime = vechime;
	}

	public String getTipAngajat() {
		return tipAngajat;
	}

	public void setTipAngajat(String tipAngajat) {
		this.tipAngajat = tipAngajat;
	}

	public Persoane getPersoane() {
		return persoane;
	}

	public void setPersoane(Persoane persoane) {
		this.persoane = persoane;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((persoane == null) ? 0 : persoane.hashCode());
		result = prime * result + ((tipAngajat == null) ? 0 : tipAngajat.hashCode());
		result = prime * result + ((vechime == null) ? 0 : vechime.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Angajati other = (Angajati) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (persoane == null) {
			if (other.persoane != null)
				return false;
		} else if (!persoane.equals(other.persoane))
			return false;
		if (tipAngajat == null) {
			if (other.tipAngajat != null)
				return false;
		} else if (!tipAngajat.equals(other.tipAngajat))
			return false;
		if (vechime == null) {
			if (other.vechime != null)
				return false;
		} else if (!vechime.equals(other.vechime))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Angajati [marca=" + marca + ", vechime=" + vechime + ", tipAngajat=" + tipAngajat + ", persoane="
				+ persoane + "]";
	}

	
	
	
	
	
	
	
	
	
	

	
	}
	
	
	
	
	
	

	
	
	
	
	

