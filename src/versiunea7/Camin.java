package versiunea7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Camine")
public class Camin implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="idCamin")
	private long idCamin;
	@Column(name="nrCamere")
	private Integer nrCamere;
	@Column(name="numeCamin")
	private String numeCamin;
	
	@ManyToOne
	@JoinColumn(name="idCampus")
	private Campus campus;
	
	@OneToMany(mappedBy="camin")
	private List<Camera> camere = new ArrayList<Camera>();
	
	
	public Camin(){}

	

	public Camin(long idCamin, Integer nrCamere, String numeCamin) {
		super();
		this.idCamin = idCamin;
		this.nrCamere = nrCamere;
		this.numeCamin = numeCamin;
	}



	public long getIdCamin() {
		return idCamin;
	}


	public void setIdCamin(long idCamin) {
		this.idCamin = idCamin;
	}
	public Integer getNrCamere() {
		return nrCamere;
	}
	public void setNrCamere(Integer nrCamere) {
		this.nrCamere = nrCamere;
	}
	public String getNumeCamin() {
		return numeCamin;
	}
	public void setNumeCamin(String numeCamin) {
		this.numeCamin = numeCamin;
	}
	public Campus getCampus() {
		return campus;
	}
	public void setCampus(Campus campus) {
		this.campus = campus;
	}
	public List<Camera> getCazari() {
		return camere;
	}
	public void setCazari(List<Camera> camere) {
		this.camere = camere;
	}


	public Camin(long idCamin, Integer nrCamere, String numeCamin, Campus campus, List<Camera> camere) {
		super();
		this.idCamin = idCamin;
		this.nrCamere = nrCamere;
		this.numeCamin = numeCamin;
		this.campus = campus;
		this.camere = camere;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((camere == null) ? 0 : camere.hashCode());
		result = prime * result + ((campus == null) ? 0 : campus.hashCode());
		result = prime * result + (int) (idCamin ^ (idCamin >>> 32));
		result = prime * result + ((nrCamere == null) ? 0 : nrCamere.hashCode());
		result = prime * result + ((numeCamin == null) ? 0 : numeCamin.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Camin other = (Camin) obj;
		if (camere == null) {
			if (other.camere != null)
				return false;
		} else if (!camere.equals(other.camere))
			return false;
		if (campus == null) {
			if (other.campus != null)
				return false;
		} else if (!campus.equals(other.campus))
			return false;
		if (idCamin != other.idCamin)
			return false;
		if (nrCamere == null) {
			if (other.nrCamere != null)
				return false;
		} else if (!nrCamere.equals(other.nrCamere))
			return false;
		if (numeCamin == null) {
			if (other.numeCamin != null)
				return false;
		} else if (!numeCamin.equals(other.numeCamin))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Camin [idCamin=" + idCamin + ", nrCamere=" + nrCamere + ", numeCamin=" + numeCamin + ", campus="
				+ campus + ", camere=" + camere + "]";
	}
	
	
	
}
