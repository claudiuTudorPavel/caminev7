package versiunea7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Studenti extends Persoane implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="numarMatricol")
	private String numarMatricol;
	@Column(name="numeStudent")
	private String numeStudent;
	@Column(name="tipMatricol")
	private String tipMatricol;
	@Column(name="anStudiuu")
	private Integer anStudiuu;
	
	@OneToOne
	@JoinColumn(name="idStudent")
	private Persoane persoane;
	
	@OneToMany(mappedBy="studenti")
	private List<Cazare> cazari = new ArrayList<Cazare>();
	
	
	
	

	

	

	

	public Studenti() {
		
	}
	
	public String getNumarMatricol() {
		return numarMatricol;
	}
	public void setNumarMatricol(String numarMatricol) {
		this.numarMatricol = numarMatricol;
	}
	public String getNumeStudent() {
		return numeStudent;
	}
	public void setNumeStudent(String numeStudent) {
		this.numeStudent = numeStudent;
	}
	public String getTipMatricol() {
		return tipMatricol;
	}
	public void setTipMatricol(String tipMatricol) {
		this.tipMatricol = tipMatricol;
	}
	public Integer getAnStudiuu() {
		return anStudiuu;
	}
	public void setAnStudiuu(Integer anStudiuu) {
		this.anStudiuu = anStudiuu;
	}
	public Persoane getPersoane() {
		return persoane;
	}
	public void setPersoane(Persoane persoane) {
		this.persoane = persoane;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((anStudiuu == null) ? 0 : anStudiuu.hashCode());
		result = prime * result + ((cazari == null) ? 0 : cazari.hashCode());
		result = prime * result + ((numarMatricol == null) ? 0 : numarMatricol.hashCode());
		result = prime * result + ((numeStudent == null) ? 0 : numeStudent.hashCode());
		result = prime * result + ((persoane == null) ? 0 : persoane.hashCode());
		result = prime * result + ((tipMatricol == null) ? 0 : tipMatricol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Studenti other = (Studenti) obj;
		if (anStudiuu == null) {
			if (other.anStudiuu != null)
				return false;
		} else if (!anStudiuu.equals(other.anStudiuu))
			return false;
		if (cazari == null) {
			if (other.cazari != null)
				return false;
		} else if (!cazari.equals(other.cazari))
			return false;
		if (numarMatricol == null) {
			if (other.numarMatricol != null)
				return false;
		} else if (!numarMatricol.equals(other.numarMatricol))
			return false;
		if (numeStudent == null) {
			if (other.numeStudent != null)
				return false;
		} else if (!numeStudent.equals(other.numeStudent))
			return false;
		if (persoane == null) {
			if (other.persoane != null)
				return false;
		} else if (!persoane.equals(other.persoane))
			return false;
		if (tipMatricol == null) {
			if (other.tipMatricol != null)
				return false;
		} else if (!tipMatricol.equals(other.tipMatricol))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Studenti [numarMatricol=" + numarMatricol + ", numeStudent=" + numeStudent + ", tipMatricol="
				+ tipMatricol + ", anStudiuu=" + anStudiuu + ", persoane=" + persoane + ", cazari=" + cazari + "]";
	}
	
	
	
	
	
	
	
}