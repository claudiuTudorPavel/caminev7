package versiunea7;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import static javax.persistence.TemporalType.DATE;
import static javax.persistence.GenerationType.AUTO;



@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Persoane implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = AUTO)
	@Column(name="idPersoane")
	private Integer idPersoane; 
	@Column(name="numePrenume")
	private String numePrenume;
	@Column(name="telefon")
	private Integer telefon;
	@Column(name="cetatenie")
	private String cetatenie;
	@Column(name="dataNastere")
	@Temporal(DATE)
	private Date dataNastere;
	
	@OneToOne(mappedBy = "persoane")
	private Angajati angajati;
	@OneToOne(mappedBy = "persoane")
	private Studenti Studenti;
	
	
	
	
	
	
	
	public Angajati getAngajati() {
		return angajati;
	}
	public void setAngajati(Angajati angajati) {
		this.angajati = angajati;
	}
	public Studenti getStudenti() {
		return Studenti;
	}
	public void setStudenti(Studenti studenti) {
		Studenti = studenti;
	}
	
	
	
	
	

	public Persoane(Integer idPersoane, String numePrenume, Integer telefon, String cetatenie, Date dataNastere,
			Angajati angajati, versiunea7.Studenti studenti) {
		super();
		this.idPersoane = idPersoane;
		this.numePrenume = numePrenume;
		this.telefon = telefon;
		this.cetatenie = cetatenie;
		this.dataNastere = dataNastere;
		this.angajati = angajati;
		Studenti = studenti;
	}
	public Persoane() {
		// TODO Auto-generated constructor stub
	}
	public Integer getIdPersoane() {
		return idPersoane;
	}
	public void setIdPersoane(Integer idPersoane) {
		this.idPersoane = idPersoane;
	}
	public String getNumePrenume() {
		return numePrenume;
	}
	public void setNumePrenume(String numePrenume) {
		this.numePrenume = numePrenume;
	}
	public Integer getTelefon() {
		return telefon;
	}
	public void setTelefon(Integer telefon) {
		this.telefon = telefon;
	}
	public String getCetatenie() {
		return cetatenie;
	}
	public void setCetatenie(String cetatenie) {
		this.cetatenie = cetatenie;
	}
	public Date getDataNastere() {
		return dataNastere;
	}
	public void setDataNastere(Date dataNastere) {
		this.dataNastere = dataNastere;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cetatenie == null) ? 0 : cetatenie.hashCode());
		result = prime * result + ((dataNastere == null) ? 0 : dataNastere.hashCode());
		result = prime * result + ((idPersoane == null) ? 0 : idPersoane.hashCode());
		result = prime * result + ((numePrenume == null) ? 0 : numePrenume.hashCode());
		result = prime * result + ((telefon == null) ? 0 : telefon.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persoane other = (Persoane) obj;
		if (cetatenie == null) {
			if (other.cetatenie != null)
				return false;
		} else if (!cetatenie.equals(other.cetatenie))
			return false;
		if (dataNastere == null) {
			if (other.dataNastere != null)
				return false;
		} else if (!dataNastere.equals(other.dataNastere))
			return false;
		if (idPersoane == null) {
			if (other.idPersoane != null)
				return false;
		} else if (!idPersoane.equals(other.idPersoane))
			return false;
		if (numePrenume == null) {
			if (other.numePrenume != null)
				return false;
		} else if (!numePrenume.equals(other.numePrenume))
			return false;
		if (telefon == null) {
			if (other.telefon != null)
				return false;
		} else if (!telefon.equals(other.telefon))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Persoane [idPersoane=" + idPersoane + ", numePrenume=" + numePrenume + ", telefon=" + telefon
				+ ", cetatenie=" + cetatenie + ", dataNastere=" + dataNastere + "]";
	}
	
	
	
	
	
	
	

}
