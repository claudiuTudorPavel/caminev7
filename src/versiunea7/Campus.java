package versiunea7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Campusuri")
public class Campus implements Serializable  
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="idCampus")
	private long idCampus;
	@Column(name="adresa")
	private String adresa;
	@Column(name="denumireCampus")
	private String denumireCampus;
	
	
	@OneToMany(mappedBy="campus")
	private List<Camin> camine = new ArrayList<Camin>();
	
	public Campus(){}
	
	

	public Campus(long idCampus, String adresa, String denumireCampus) {
		super();
		this.idCampus = idCampus;
		this.adresa = adresa;
		this.denumireCampus = denumireCampus;
	}



	public long getIdCampus() {
		return idCampus;
	}

	public void setIdCampus(long idCampus) {
		this.idCampus = idCampus;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getDenumireCampus() {
		return denumireCampus;
	}

	public void setDenumireCampus(String denumireCampus) {
		this.denumireCampus = denumireCampus;
	}

	public List<Camin> getCamine() {
		return camine;
	}

	public void setCamine(List<Camin> camine) {
		this.camine = camine;
	}

	public Campus(long idCampus, String adresa, String denumireCampus, List<Camin> camine) {
		super();
		this.idCampus = idCampus;
		this.adresa = adresa;
		this.denumireCampus = denumireCampus;
		this.camine = camine;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresa == null) ? 0 : adresa.hashCode());
		result = prime * result + ((camine == null) ? 0 : camine.hashCode());
		result = prime * result + ((denumireCampus == null) ? 0 : denumireCampus.hashCode());
		result = prime * result + (int) (idCampus ^ (idCampus >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campus other = (Campus) obj;
		if (adresa == null) {
			if (other.adresa != null)
				return false;
		} else if (!adresa.equals(other.adresa))
			return false;
		if (camine == null) {
			if (other.camine != null)
				return false;
		} else if (!camine.equals(other.camine))
			return false;
		if (denumireCampus == null) {
			if (other.denumireCampus != null)
				return false;
		} else if (!denumireCampus.equals(other.denumireCampus))
			return false;
		if (idCampus != other.idCampus)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Campus [idCampus=" + idCampus + ", adresa=" + adresa + ", denumireCampus=" + denumireCampus
				+ ", camine=" + camine + "]";
	}
	
	
	
	

}
