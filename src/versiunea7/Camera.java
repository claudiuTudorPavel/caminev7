package versiunea7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Camere")
public class Camera implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="idCamera")
	private Integer idCamera;
	@Column(name="capacitate")
	private Double capacitate;
	@Column(name="nrCamera")
	private Double nrCamera;
	@Column(name="Observatii")
	private String Observatii;
	
	@ManyToOne
	@JoinColumn(name="idCamin")
	private Camin camin;
	
	@OneToMany(mappedBy="camera")
	private List<Cazare> cazari = new ArrayList<Cazare>(); 
	
	public Camera(){}

	public Integer getIdCamera() {
		return idCamera;
	}
	public void setIdCamera(Integer idCamera) {
		this.idCamera = idCamera;
	}
	public Double getCapacitate() {
		return capacitate;
	}
	public void setCapacitate(Double capacitate) {
		this.capacitate = capacitate;
	}
	public Double getNrCamera() {
		return nrCamera;
	}
	public void setNrCamera(Double nrCamera) {
		this.nrCamera = nrCamera;
	}
	public String getObservatii() {
		return Observatii;
	}
	public void setObservatii(String observatii) {
		Observatii = observatii;
	}
	public List<Cazare> getCazari() {
		return cazari;
	}
	public void setCazari(List<Cazare> cazari) {
		this.cazari = cazari;
	}

	public Camera(Integer idCamera, Double capacitate, Double nrCamera, String observatii, List<Cazare> cazari) {
		super();
		this.idCamera = idCamera;
		this.capacitate = capacitate;
		this.nrCamera = nrCamera;
		Observatii = observatii;
		this.cazari = cazari;
	}

	public Camera(Integer valueOf, Object object) 
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Observatii == null) ? 0 : Observatii.hashCode());
		result = prime * result + ((camin == null) ? 0 : camin.hashCode());
		result = prime * result + ((capacitate == null) ? 0 : capacitate.hashCode());
		result = prime * result + ((cazari == null) ? 0 : cazari.hashCode());
		result = prime * result + (int) (idCamera ^ (idCamera >>> 32));
		result = prime * result + ((nrCamera == null) ? 0 : nrCamera.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Camera other = (Camera) obj;
		if (Observatii == null) {
			if (other.Observatii != null)
				return false;
		} else if (!Observatii.equals(other.Observatii))
			return false;
		if (camin == null) {
			if (other.camin != null)
				return false;
		} else if (!camin.equals(other.camin))
			return false;
		if (capacitate == null) {
			if (other.capacitate != null)
				return false;
		} else if (!capacitate.equals(other.capacitate))
			return false;
		if (cazari == null) {
			if (other.cazari != null)
				return false;
		} else if (!cazari.equals(other.cazari))
			return false;
		if (idCamera != other.idCamera)
			return false;
		if (nrCamera == null) {
			if (other.nrCamera != null)
				return false;
		} else if (!nrCamera.equals(other.nrCamera))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Camera [idCamera=" + idCamera + ", capacitate=" + capacitate + ", nrCamera=" + nrCamera
				+ ", Observatii=" + Observatii + ", camin=" + camin + ", cazari=" + cazari + "]";
	}
	
	
	
	

}
