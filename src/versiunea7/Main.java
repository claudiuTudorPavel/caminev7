package versiunea7;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		EntityManagerFactory emf;
		EntityManager em;
		
		emf = Persistence.createEntityManagerFactory("CamineV7");
		em = emf.createEntityManager();
		
		Studenti stud = new Studenti();
		stud.setNumarMatricol("7777sl777");
		stud.setNumeStudent("Ionescu");
		stud.setTipMatricol("permanent");
		stud.setAnStudiuu(1);
		
		Studenti stud2 = new Studenti();
		stud2.setNumarMatricol("7777sl888");
		stud2.setNumeStudent("Popescu");
		stud2.setTipMatricol("permanent");
		stud2.setAnStudiuu(1);
		
		Camin c1 = new Camin(12345L,450,"Copou");
		Camin c2 = new Camin(23456L,450,"Targusor");
		Camin c3 = new Camin(34567L,450,"Academos");
		Camin c4 = new Camin(45678L,450,"Tudor");
		
		Campus cp1 = new Campus(121212l,"Alea Rozelor","Copu");
		Campus cp2 = new Campus(232323l,"Alea Trandafirilor","Tudor");
		Campus cp3 = new Campus(474747l,"Alea Ghiocelului","Breazu");
		
		Camin camin = new Camin (56789L,160,"Canta");
		
		
		
		
		em.getTransaction().begin();
		em.persist(stud);
		em.persist(stud2);
		em.persist(c1);
		em.persist(c2);
		em.persist(c3);
		em.persist(c4);
		em.persist(cp1);
		em.persist(cp2);
		em.persist(cp3);
		em.persist(camin);
		em.getTransaction().commit();
		

	}

}
